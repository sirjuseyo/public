## 이미지 올리는 방법

1. 이미지를 용도에 맞는 경로 확인하고 업로드 한다.

   1. 배너 이미지
      - https://gitlab.com/albit-public/public/tree/master/banners/images
   2. 웰컴팝업 이미지
      - https://gitlab.com/albit-public/public/tree/master/public/images

2. 올린 이미지의 URL 을 따서 용도에 맞게 사용하기.
   - 이미지를 주소창에 드래그 드랍하면 나옴.
   - 예시 -> https://gitlab.com/albit-public/public/raw/master/banners/images/xxx.xxx
   - /public/ 폴더 안의 내용은 아래 url 형식으로 사용 가능함.
     https://albit-public.gitlab.io/public/images/sample.jpg

## 하단 배너 데이타 작성 하는 방법

1. 공지사항을 작성 in admin web page
2. 공지사항의 id 를 따로 적어둔다. - 게시물 좌측의 번호 -
3. 연결할 배너 이미지를 업로드 한다.
   - https://gitlab.com/albit-public/public/tree/master/banners/images
4. 올린 이미지의 URL 을 따온다.
   - 이미지를 주소창에 드래그 드랍하면 나옴.
   - 예시 -> https://gitlab.com/albit-public/public/raw/master/banners/images/xxx.xxx
5. 배너 데이타 파일을 편집한다.
   - https://gitlab.com/albit-public/public/edit/master/banners/index.json
6. 형식에 맞추어 배너와 공지사항 내용을 작성한다. 기존 아이템을 복사하면 편리함.
   모두 작성 후, 하단의 'Commit changes' 클릭

```
{
  "image": "https://url-to-banner-image.jpg",
  "action": "OpenNotice",
  "data": id-for-this-notice
},

```

## 공지사항 링크 만드는 방법

1. 아래 위치에 공지 내용을 작성하여 올린다.

- https://gitlab.com/albit-public/public/tree/master/public/notices
- 내용 (html)을 아래 틀에 붙여서 작성한다. (~~~ 을 바꿔치기 한다.)

```
<!DOCTYPE html>
<html>
  <head>
    <meta name="viewport" content="width=device-width, initial-scale=1"/>
  </head>
  <body style="padding:5%">

    ~~~

  </body>
</html>
```

- 모두 작성 후, 하단의 'Commit changes' 클릭

2. url 형식은 이렇다.

```
  https://albit-public.gitlab.io/public/notices/notice-sample.html
```

## 웰컴팝업 만들기

1. FirebaseRemoteConfig Open
   https://console.firebase.google.com/u/0/project/admob-app-id-1911172394/config
2. 경로 - welcomePopupItems/items

```sample
    {
      "title": "단기일자리 자세히 보기",
      "titleBase64Encoded": "64uo6riw7J287J6Q66asIOyekOyEuO2eiCDrs7TquLA=",
      "image": "https://firebasestorage.googleapis.com/v0/b/admob-app-id-1911172394.appspot.com/o/event-images%2Fevent-hire-testers.png?alt=media&token=1c64cd0a-f183-4876-a095-b9c43519e8ea",
      "link": "https://albit-public.gitlab.io/public/notices/gig-21.html",
      "begin": "2019.12.17 00:00:00",
      "end": "2019.12.23 23:59:59",
      "platforms": [
        "android",
        "iOS"
      ]
    },
```

- title: iOS 전용 제목
- titleBase66Encoded: android 전용 제목 ()
  - Base64 Encode 사이트 -> http://www.utilities-online.info/base64/
    - 좌측칸에 문자열 넣고, encode--> 버튼 누른 다음 인코딩된 우측의 문자열을 가져다 쓴다.
- image: 이미지 경로
- link: 버튼 누를 경우 이동되는 경로
- begin: 게시 시작 시간
- end: 게시 종료 시간
- platforms: ["android", "iOS"] // 표기하는 플랫폼에만 게시됨
